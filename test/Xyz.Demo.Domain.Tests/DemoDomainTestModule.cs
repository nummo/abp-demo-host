﻿using Volo.Abp.Modularity;
using Xyz.Demo.EntityFrameworkCore;

namespace Xyz.Demo
{
	[DependsOn(
		typeof(DemoEntityFrameworkCoreTestModule)
	)]
	public class DemoDomainTestModule: AbpModule
	{
	}
}
