﻿using Volo.Abp.Modularity;

namespace Xyz.Demo
{
	[DependsOn(
		typeof(DemoApplicationModule),
		typeof(DemoDomainTestModule)
	)]
	public class DemoApplicationTestModule: AbpModule
	{
	}
}
