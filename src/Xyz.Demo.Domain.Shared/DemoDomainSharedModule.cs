﻿using Volo.Abp.AuditLogging;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.IdentityServer;
using Volo.Abp.Localization;
using Volo.Abp.Localization.Resources.AbpValidation;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.SettingManagement;
using Volo.Abp.TenantManagement;
using Volo.Abp.VirtualFileSystem;
using Xyz.Demo.Localization;

namespace Xyz.Demo
{
	[DependsOn(
		typeof(AbpAuditLoggingDomainSharedModule),
		typeof(AbpBackgroundJobsDomainSharedModule),
		typeof(AbpFeatureManagementDomainSharedModule),
		typeof(AbpIdentityDomainSharedModule),
		typeof(AbpIdentityServerDomainSharedModule),
		typeof(AbpPermissionManagementDomainSharedModule),
		typeof(AbpSettingManagementDomainSharedModule),
		typeof(AbpTenantManagementDomainSharedModule)
	)]
	public class DemoDomainSharedModule: AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			Configure<AbpVirtualFileSystemOptions>(options =>
				{
					options.FileSets.AddEmbedded<DemoDomainSharedModule>("Xyz.Demo");
				});

			Configure<AbpLocalizationOptions>(options =>
				{
					options.Resources
						.Add<DemoResource>("en")
						.AddBaseTypes(typeof(AbpValidationResource))
						.AddVirtualJson("/Localization/Demo");
				});
		}
	}
}
