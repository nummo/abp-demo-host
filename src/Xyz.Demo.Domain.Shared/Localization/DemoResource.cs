﻿using Volo.Abp.Localization;

namespace Xyz.Demo.Localization
{
	[LocalizationResourceName("Demo")]
	public class DemoResource
	{
	}
}
