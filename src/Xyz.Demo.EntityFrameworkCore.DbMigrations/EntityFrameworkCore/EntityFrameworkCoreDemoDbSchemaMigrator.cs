﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.DependencyInjection;
using Xyz.Demo.Data;

namespace Xyz.Demo.EntityFrameworkCore
{
	[Dependency(ReplaceServices = true)]
	public class EntityFrameworkCoreDemoDbSchemaMigrator
		: IDemoDbSchemaMigrator, ITransientDependency
	{
		private readonly DemoMigrationsDbContext _dbContext;

		public EntityFrameworkCoreDemoDbSchemaMigrator(DemoMigrationsDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task MigrateAsync()
		{
			await _dbContext.Database.MigrateAsync();
		}
	}
}
