﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Xyz.Demo.Web
{
	public class Startup
	{
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddApplication<DemoWebModule>();
		}

		public void Configure(IApplicationBuilder app)
		{
			app.InitializeApplication();
		}
	}
}
