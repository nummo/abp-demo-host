﻿using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Xyz.Demo.Localization;

namespace Xyz.Demo.Web.Pages
{
	/* Inherit your PageModel classes from this class.
	 */
	public abstract class DemoPageModel: AbpPageModel
	{
		protected DemoPageModel()
		{
			LocalizationResourceType = typeof(DemoResource);
		}
	}
}
