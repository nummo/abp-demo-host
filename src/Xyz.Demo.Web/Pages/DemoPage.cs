﻿using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Xyz.Demo.Localization;

namespace Xyz.Demo.Web.Pages
{
	/* Inherit your UI Pages from this class. To do that, add this line to your Pages (.cshtml files under the Page folder):
	 * @inherits Xyz.Demo.Web.Pages.DemoPage
	 */
	public abstract class DemoPage: AbpPage
	{
		[RazorInject]
		public IHtmlLocalizer<DemoResource> L { get; set; }
	}
}
