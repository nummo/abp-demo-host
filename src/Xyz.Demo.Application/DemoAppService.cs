﻿using Volo.Abp.Application.Services;
using Xyz.Demo.Localization;

namespace Xyz.Demo
{
	/* Inherit your application services from this class.
	 */
	public abstract class DemoAppService: ApplicationService
	{
		protected DemoAppService()
		{
			LocalizationResource = typeof(DemoResource);
		}
	}
}
