﻿using Volo.Abp.AspNetCore.Mvc;
using Xyz.Demo.Localization;

namespace Xyz.Demo.Controllers
{
	/* Inherit your controllers from this class.
	 */
	public abstract class DemoController: AbpController
	{
		protected DemoController()
		{
			LocalizationResource = typeof(DemoResource);
		}
	}
}
