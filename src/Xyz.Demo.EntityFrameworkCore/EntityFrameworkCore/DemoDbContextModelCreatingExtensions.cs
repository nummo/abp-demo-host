﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Volo.Abp;
using Volo.Abp.Users;

namespace Xyz.Demo.EntityFrameworkCore
{
	public static class DemoDbContextModelCreatingExtensions
	{
		public static void ConfigureDemo(this ModelBuilder builder)
		{
			Check.NotNull(builder, nameof(builder));

			/* Configure your own tables/entities inside here */

			//builder.Entity<YourEntity>(b =>
			//{
			//    b.ToTable(DemoConsts.DbTablePrefix + "YourEntities", DemoConsts.DbSchema);

			//    //...
			//});
		}

		public static void ConfigureCustomUserProperties<TUser>(this EntityTypeBuilder<TUser> b)
			where TUser: class, IUser
		{
			//b.Property<string>(nameof(AppUser.MyProperty))...
		}
	}
}
