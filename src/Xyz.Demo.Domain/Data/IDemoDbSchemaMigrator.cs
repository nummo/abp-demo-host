﻿using System.Threading.Tasks;

namespace Xyz.Demo.Data
{
	public interface IDemoDbSchemaMigrator
	{
		Task MigrateAsync();
	}
}
