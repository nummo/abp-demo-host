﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;

namespace Xyz.Demo.Data
{
	public class DemoDbMigrationService: ITransientDependency
	{
		private readonly IDataSeeder _dataSeeder;
		private readonly IDemoDbSchemaMigrator _dbSchemaMigrator;

		public DemoDbMigrationService(
			IDataSeeder dataSeeder,
			IDemoDbSchemaMigrator dbSchemaMigrator)
		{
			_dataSeeder = dataSeeder;
			_dbSchemaMigrator = dbSchemaMigrator;

			Logger = NullLogger<DemoDbMigrationService>.Instance;
		}

		public ILogger<DemoDbMigrationService> Logger { get; set; }

		public async Task MigrateAsync()
		{
			Logger.LogInformation("Started database migrations...");

			Logger.LogInformation("Migrating database schema...");
			await _dbSchemaMigrator.MigrateAsync();

			Logger.LogInformation("Executing database seed...");
			await _dataSeeder.SeedAsync();

			Logger.LogInformation("Successfully completed database migrations.");
		}
	}
}
