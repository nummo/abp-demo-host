﻿using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Xyz.Demo.Localization;

namespace Xyz.Demo.Permissions
{
	public class DemoPermissionDefinitionProvider: PermissionDefinitionProvider
	{
		public override void Define(IPermissionDefinitionContext context)
		{
			var myGroup = context.AddGroup(DemoPermissions.GroupName);

			//Define your own permissions here. Example:
			//myGroup.AddPermission(DemoPermissions.MyPermission1, L("Permission:MyPermission1"));
		}

		private static LocalizableString L(string name)
		{
			return LocalizableString.Create<DemoResource>(name);
		}
	}
}
