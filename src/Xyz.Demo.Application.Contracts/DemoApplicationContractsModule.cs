﻿using Volo.Abp.Account;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.TenantManagement;

namespace Xyz.Demo
{
	[DependsOn(
		typeof(DemoDomainSharedModule),
		typeof(AbpAccountApplicationContractsModule),
		typeof(AbpFeatureManagementApplicationContractsModule),
		typeof(AbpIdentityApplicationContractsModule),
		typeof(AbpPermissionManagementApplicationContractsModule),
		typeof(AbpTenantManagementApplicationContractsModule)
	)]
	public class DemoApplicationContractsModule: AbpModule
	{
	}
}
